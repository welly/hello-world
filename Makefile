APP_ROOT=./public
BUILD_LOGS_DIR=./build/logs
PHPCS_FOLDERS=$(APP_ROOT)/modules/custom
PHPCS_EXTENSIONS=php,module,inc,install,test,profile,theme

phpcs:
	./bin/phpcs --report=full --standard=vendor/drupal/coder/coder_sniffer/Drupal/ruleset.xml --extensions=$(PHPCS_EXTENSIONS) $(PHPCS_FOLDERS)
